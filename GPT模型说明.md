# 目录

<!-- TOC -->

- [GPT-2模型](#GPT-2模型)
- [模型架构](#模型架构)
- [脚本说明](#脚本说明)
    - [模型转换](#模型转换)
    - [数据预处理](#数据预处理)
    - [预训练](#预训练)
    - [下游任务微调](#下游任务微调)
    - [评测](#评测)

<!-- /TOC -->

# GPT-3模型

GPT-2由Open于2019年发布。GPT-2模型是继承于GPT模型，GPT-2是一个非常庞大的语言模型，它主要是用于预测下一个单词。按照参数量的大小，GPT-2模型可分为small（117M）、medium（345M）、large（762M）、xlarge（1542M）。

[GPT-2论文](https://d4mucfpksywv.cloudfront.net/better-language-models/language-models.pdf): Radford, A., Wu, J., Child, R., Luan, D., Amodei, D., & Sutskever, I. (2019). Language models are unsupervised multitask learners. OpenAI blog, 1(8), 9.

# 模型架构

GPT-2模型由Transformer的解码器实现，Transformer包括多个编码器层和多个解码器层，但在GPT-2模型中仅使用了Transformer的解码器部分。
微调时，根据不同的任务，采用不同的数据集对预训练的模型进行微调。
测试过程中，通过微调后的模型预测结果，对于某些任务可以直接进行zero-shot评估即可。

# 脚本说明

## 模型转换

- 下载[GPT-2预训练模型](https://huggingface.co/gpt2/blob/main/pytorch_model.bin)
- 运行`convert_weight.py`，示例代码如下
```bash
cd mindformers/models/gpt
python convert_weight.py --layers {layer_num} --torch_path {torch_path} --mindspore_path {mindspore_path}
``` 

## 数据预处理
注：当前tokenizer针对原词表  
以wikitext2为例:
- 下载数据集[WikiText2数据集下载](https://s3.amazonaws.com/research.metamind.io/wikitext/wikitext-2-v1.zip)
- 使用`task_dataset_preprocess.py`对以上数据集进行清洗，示例代码如下：
```bash
cd mindformers/dataset/gpt_data_preprocess
python task_dataset_preprocess.py --task "LanguageModeling" --input_file {wikitext2} --dataset "wikitext2" --output_file {wikitext2_cleaned_data_path}
```
- 使用`create_lm_data.py`生成mindrecord格式的数据（生成的数据名称以mindrecord结尾），示例代码如下
```bash
cd mindformers/dataset/gpt_data_preprocess
# max_length：预训练时为1025，下游lm任务时为1024
python create_lm_data.py --input_file {wikitext2_cleaned_data_path} --output_file {wikitext2_mindrecord_path} --num_splits 1 --max_length 1024 --vocab_file {vocab_file_path} --merge_file {merge_file_path}
```

## 预训练
```bash
python run_mindformer.py --config configs/gpt/run_gpt_2.yaml --run_status train --dataset {dataset_file_path} --dataset_id {dataset_name} --load_checkpoint {pretrained_model_path} --use_parallel True
```

## 下游任务finetune
```bash
python run_mindformer.py --config configs/gpt/run_gpt_2_lm.yaml --run_status finetune --dataset {dataset_file_path} --dataset_id {dataset_name} --load_checkpoint {pretrained_model_path} --use_parallel True
```

## 下游任务评测
```bash
python run_mindformer.py --config configs/gpt/run_gpt_2_lm.yaml --run_status eval --dataset {dataset_file_path} --dataset_id {dataset_name} --load_checkpoint {pretrained_model_path（相对路径）} --use_parallel True
```