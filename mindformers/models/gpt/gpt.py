# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

"""GPT model"""
import numpy as np
import copy
import mindspore.nn as nn
import mindspore.common.dtype as mstype
from mindspore.common.initializer import TruncatedNormal, initializer
from mindspore.ops import operations as P
from mindspore.ops import functional as F
from mindspore.nn.transformer.transformer import default_moe_config
from mindspore.nn.transformer.layers import _LayerNorm
from mindspore.nn.transformer.transformer import AttentionMask, Transformer, VocabEmbedding
from mindspore.nn.transformer.loss import CrossEntropyLoss
from mindformers.tools.register import MindFormerRegister, MindFormerModuleType
from mindformers.models.base_model import BaseModel
from ...mindformer_book import MindFormerBook
from .gpt_config import GptConfig

__all__ = ['GptForPretraining']

@MindFormerRegister.register(MindFormerModuleType.MODELS)
class GptForPretraining(BaseModel):
    """
        Provide ogt pre-training loss through network.

        Args:
            config (GptConfig): The config of GptModel.
            is_training (bool): Specifies whether to use the training mode.

        Returns:
            Tensor, the loss of the network.
    """
    _support_list = MindFormerBook.get_model_support_list()['gpt']

    def __init__(self, config=GptConfig()):
        super(GptForPretraining, self).__init__(config)
        self.gpt2 = GPT(config, config.is_training)
        self.is_training = config.is_training
        self.loss = GPTLoss(config)
        self.cast = P.Cast()
        self.add = P.Add().shard(((1,), ()))
        self.use_moe = (config.parallel_config.moe_config.expert_num > 1)
        self.eos_token = config.eos_token
        parallel_config = config.parallel_config
        dp = parallel_config.data_parallel
        self.stridedslice = P.StridedSlice().shard(((dp, 1),))
        self.not_equal = P.NotEqual().shard(((dp, 1), ()))
        self.shape = P.Shape()

    def construct(self, input_ids):
        """Get pre-training loss"""
        batch_size, seq_length = self.shape(input_ids)
        tokens = self.stridedslice(input_ids, (0, 0), (batch_size, seq_length - 1), (1, 1))
        masks = F.cast(self.not_equal(tokens, self.eos_token), mstype.float32)
        labels = self.stridedslice(input_ids, (0, 1), (batch_size, seq_length), (1, 1))
        logits, moe_loss = self.gpt2(tokens, masks)
        loss = self.loss(logits, labels, masks)
        if not self.is_training:
            return logits
        if not self.use_moe:
            return self.cast(loss, mstype.float32)
        total_loss = self.add(loss, moe_loss)
        return self.cast(total_loss, mstype.float32)


class SaturateCast(nn.Cell):
    """
    Performs a safe saturating cast. This operation applies proper clamping before casting to prevent
    the danger that the value will overflow or underflow.

    Args:
        src_type (:class:`mindspore.dtype`): The type of the elements of the input tensor. Default: mstype.float32.
        dst_type (:class:`mindspore.dtype`): The type of the elements of the output tensor. Default: mstype.float32.
    """
    def __init__(self, dst_type=mstype.float32):
        super(SaturateCast, self).__init__()
        np_type = mstype.dtype_to_nptype(dst_type)

        self.tensor_min_type = float(np.finfo(np_type).min)
        self.tensor_max_type = float(np.finfo(np_type).max)

        self.min_op = P.Minimum()
        self.max_op = P.Maximum()
        self.cast = P.Cast()
        self.dst_type = dst_type

    def construct(self, x):
        out = self.max_op(x, self.tensor_min_type)
        out = self.min_op(out, self.tensor_max_type)
        return self.cast(out, self.dst_type)


class GPTModel(nn.Cell):
    """
    The backbone of GPT network

    Args:
        config(GPTConfig): the config of network

    Inputs:
        input_ids: the tokenized inputs with datatype int32
        input_mask: the mask indicating whether each position is a valid input
        layer_past: the previous feature map

    Returns:
        output_state: Tensor, the output logit of backbone
        present_layer: Tensor, the current feature map
        embedding_table: Tensor, the embedding table for the vocabulary
    """

    def __init__(self, config):
        super(GPTModel, self).__init__()
        self.get_attention_mask = AttentionMask(seq_length=config.seq_length,
                                                parallel_config=config.parallel_config.dp_mp_config)
        self.word_embedding = VocabEmbedding(vocab_size=config.vocab_size,
                                             embedding_size=config.hidden_size,
                                             param_init=initializer("truncatedNormal",
                                                                    [config.vocab_size, config.hidden_size],
                                                                    dtype=config.compute_dtype),
                                             parallel_config=config.parallel_config.embedding_dp_mp_config)

        new_parallel_config = copy.deepcopy(config.parallel_config.embedding_dp_mp_config)
        new_parallel_config.vocab_emb_dp = True

        self.position_embedding = VocabEmbedding(vocab_size=config.seq_length,
                                                 embedding_size=config.hidden_size,
                                                 param_init=initializer(TruncatedNormal(0.02),
                                                                        [config.seq_length, config.hidden_size],
                                                                        dtype=config.compute_dtype),
                                                 parallel_config=new_parallel_config)
        self.blocks = nn.CellList()
        if not hasattr(config.parallel_config, "moe_config"):
            config.parallel_config.moe_config = default_moe_config
        moe_config = config.parallel_config.moe_config
        self.transformer = Transformer(hidden_size=config.hidden_size,
                                       batch_size=config.batch_size,
                                       ffn_hidden_size=config.hidden_size * config.expand_ratio,
                                       src_seq_length=config.seq_length,
                                       tgt_seq_length=config.seq_length,
                                       encoder_layers=config.num_layers,
                                       attention_dropout_rate=config.attention_probs_dropout_prob,
                                       hidden_dropout_rate=config.hidden_dropout_prob,
                                       decoder_layers=0,
                                       param_init_type=config.compute_dtype,
                                       layernorm_compute_type=config.layernorm_dtype,
                                       softmax_compute_type=config.softmax_dtype,
                                       num_heads=config.num_heads,
                                       parallel_config=config.parallel_config,
                                       moe_config=moe_config)
        self.cast = P.Cast()
        self.dtype = config.dtype
        self.cast_compute_type = SaturateCast(dst_type=config.compute_dtype)
        self.use_moe = (moe_config.expert_num > 1)
        self.layernorm = _LayerNorm((config.hidden_size,)).to_float(config.layernorm_dtype)
        self.layernorm.shard(((config.parallel_config.data_parallel, 1, 1),))
        self.add = P.Add().shard(
            ((config.parallel_config.data_parallel, 1, 1), (config.parallel_config.data_parallel, 1, 1)))

    def construct(self, input_ids, input_mask):
        """GPT model"""
        input_embedding, embedding_table = self.word_embedding(input_ids)

        batch_size, seq_length = F.shape(input_ids)
        input_position = F.tuple_to_array(F.make_range(seq_length))
        input_position = P.Tile()(input_position, (batch_size, 1))

        position_embedding, _ = self.position_embedding(input_position)
        hidden_states = self.add(input_embedding, position_embedding)
        input_mask = P.Cast()(input_mask, self.dtype)
        attention_mask = self.get_attention_mask(input_mask)

        moe_loss = 0
        if self.use_moe:
            hidden_states, present_layer, _, moe_loss = self.transformer(self.cast_compute_type(hidden_states),
                                                                         attention_mask)
        else:
            hidden_states, present_layer, _ = self.transformer(self.cast_compute_type(hidden_states), attention_mask)

        output_state = self.layernorm(hidden_states)

        if self.use_moe:
            return output_state, present_layer, embedding_table, moe_loss
        return output_state, present_layer, embedding_table


class GPTHead(nn.Cell):
    """
    Head for GPT to get the logits of each token in the vocab

    Args:
        config(GPTConfig): the config of network

    Inputs:
        state: the output of the backbone
        embedding_table: the embedding table of the vocabulary

    Returns:
        logits: Tensor, the logits of the corresponding inputs
    """

    def __init__(self,
                 hidden_size,
                 compute_type=mstype.float16,
                 parallel_config=None):
        super(GPTHead, self).__init__()
        if parallel_config.vocab_emb_dp:
            self.matmul = P.MatMul(transpose_b=True).shard(((parallel_config.data_parallel, 1), (1, 1)))
        else:
            self.matmul = P.MatMul(transpose_b=True).shard(((parallel_config.data_parallel, 1), (
                parallel_config.model_parallel, 1)))
        self.hidden_size = hidden_size
        self.dtype = compute_type
        self.cast = P.Cast()

    def construct(self, state, embedding_table):
        state = P.Reshape()(state, (-1, self.hidden_size))
        logits = self.matmul(self.cast(state, self.dtype), self.cast(embedding_table, self.dtype))
        return logits


class GPT(nn.Cell):
    """
    The GPT network consisting of two parts the backbone and the head

    Args:
        config(GPTConfig): the config of network

    Inputs:
        input_ids: the tokenized inputs
        input_mask: the mask indicating whether each position is a valid input

    Returns:
        logits: Tensor: the logits of the corresponding inputs with shape (batch_size, seq_length, vocab_size)
    """

    def __init__(self, config, is_training=None):
        super(GPT, self).__init__()
        if not is_training:
            config.hidden_dropout_prob = 0.0
            config.attention_probs_dropout_prob = 0.0
        self.backbone = GPTModel(config)
        self.head = GPTHead(config.hidden_size, parallel_config=config.parallel_config)
        self.use_moe = self.backbone.use_moe

    def construct(self, input_ids, input_mask):
        moe_loss = 0.0
        if self.use_moe:
            output_states, _, embedding_table, moe_loss = self.backbone(input_ids, input_mask)
            logits = self.head(output_states, embedding_table)
            return logits, moe_loss
        output_states, _, embedding_table = self.backbone(input_ids, input_mask)
        logits = self.head(output_states, embedding_table)
        return logits, moe_loss


class GPTLoss(nn.Cell):
    """Get loss of GPT"""
    def __init__(self, config):
        super(GPTLoss, self).__init__()
        parallel_config = config.parallel_config

        if parallel_config.model_parallel != 1:
            parallel_config.data_parallel = parallel_config.data_parallel * parallel_config.model_parallel
            parallel_config.model_parallel = 1

        self.loss = CrossEntropyLoss(parallel_config.dp_mp_config)

    def construct(self, logits, labels, input_mask):
        labels = P.Reshape()(labels, (-1,))
        input_mask = P.Reshape()(input_mask, (-1,))
        output = self.loss(logits, labels, input_mask)
        return output
