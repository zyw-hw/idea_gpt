# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Masked Image Modeling Trainer."""
import math
from typing import Optional, List, Union

from mindspore.train.model import Model
from mindspore.train import Callback
from mindspore.nn import TrainOneStepCell, Optimizer
from mindspore.dataset import GeneratorDataset

from mindformers.common.callback import build_callback
from mindformers.dataset import build_dataset, check_dataset_config, BaseDataset
from mindformers.models import build_model, BaseModel, BertTokenizer, BaseTokenizer, BertConfig
from mindformers.models import GptTokenizer
from mindformers.common.lr import WarmUpDecayLR
from mindformers.common.optim import build_optim
from mindformers.wrapper import build_wrapper
from mindformers.tools.logger import logger
from mindformers.tools.utils import count_params
from mindformers.tools.register import MindFormerRegister, MindFormerModuleType
from mindformers.pipeline import pipeline
from ...dataset.dataloader import build_dataset_loader
from ..config_args import ConfigArguments
from ..base_trainer import BaseTrainer
from ..utils import check_runner_config, resume_checkpoint_for_training
from mindspore.train.serialization import load_checkpoint, load_param_into_net


@MindFormerRegister.register(MindFormerModuleType.TRAINER, alias="gpt")
class GptLanguageModelingTrainer(BaseTrainer):
    r"""MaskedLanguageModeling Task For Trainer.
    Args:
        model_name (str): The model name of Task-Trainer. Default: None
    Raises:
        NotImplementedError: If train method or evaluate method or predict method not implemented.
    """
    def __init__(self, model_name: str = None):
        super(GptLanguageModelingTrainer, self).__init__(model_name)
        self.kwargs = None

    def set_weight_decay(self, params):
        """
        Set weight decay coefficient, zero for bias and layernorm, 1e-1 for rest
        """
        decay_filter = lambda x: 'layernorm' not in x.name.lower() and "bias" not in x.name.lower()
        decay_params = list(filter(decay_filter, params))
        other_params = list(filter(lambda x: not decay_filter(x), params))
        group_params = [{
            'params': decay_params,
            'weight_decay': 1e-1
        }, {
            'params': other_params,
            'weight_decay': 0.0
        }, {
            'order_params': params
        }]
        return group_params

    def get_checkpoint_name_or_path(self, config):
        checkpoint_name_or_path = ""
        try:
            checkpoint_name_or_path = config.model.model_config.checkpoint_name_or_path
        except:
            pass
        return checkpoint_name_or_path

    def train(self,
              config: Optional[Union[dict, ConfigArguments]] = None,
              network: Optional[Union[str, BaseModel]] = None,
              dataset: Optional[Union[str, BaseDataset]] = None,
              wrapper: Optional[TrainOneStepCell] = None,
              optimizer: Optional[Optimizer] = None,
              callbacks: Optional[Union[Callback, List[Callback]]] = None,
              **kwargs):
        r"""Train task for MaskedImageModeling Trainer.
        This function is used to train or fine-tune the network.

        The trainer interface is used to quickly start training for general task.
        It also allows users to customize the network, optimizer, dataset, wrapper, callback.

        Args:
            config (Optional[Union[dict, ConfigArguments]]): The task config which is used to
                configure the dataset, the hyper-parameter, optimizer, etc.
                It support config dict or ConfigArguments class.
                Default: None.
            network (Optional[Union[str, BaseModel]]): The network for trainer. It support model name supported
                or BaseModel class. Supported model name can refer to ****.
                Default: None.
            dataset (Optional[Union[str, BaseDataset]]): The training dataset. It support real dataset path or
                BaseDateset class or MindSpore Dataset class.
                Default: None.
            optimizer (Optional[Optimizer]): The training network's optimizer. It support Optimizer class of MindSpore.
                Default: None.
            wrapper (Optional[TrainOneStepCell]): Wraps the `network` with the `optimizer`.
                It support TrainOneStepCell class of MindSpore.
                Default: None.
            callbacks (Optional[Union[Callback, List[Callback]]]): The training callback function.
                It support CallBack or CallBack List of MindSpore.
                Default: None.

        Raises:
            NotImplementedError: If wrapper not implemented.
        """
        # DIY model training, TODO
        self.kwargs = kwargs
        # build dataset
        logger.info(".........Build Dataset..........")
        check_dataset_config(config)
        if dataset is None:
            dataset = build_dataset(config.train_dataset_task)
        check_runner_config(config, dataset)
        step_per_epoch = dataset.get_dataset_size()
        total_steps = config.runner_config.epochs * step_per_epoch

        # build network
        logger.info(".........Build Net..........")
        if network is None:
            network = build_model(config.model, default_args={
                "parallel_config": config.parallel_config,
                "moe_config": config.moe_config})
        logger.info("Network Parameters: %s M.", str(count_params(network)))

        # build optimizer
        logger.info(".........Build Optimizer..........")
        if optimizer is None:
            # build learning rate schedule
            logger.info(".........Build LR Schedule..........")
            warmup_steps = config.lr_schedule.warmup_steps if config.lr_schedule.warmup_steps > 0 \
                else int(0.1 * total_steps)
            lr_schedule = WarmUpDecayLR(learning_rate=float(config.lr_schedule.learning_rate),
                                        end_learning_rate=float(config.lr_schedule.end_learning_rate),
                                        warmup_steps=warmup_steps,
                                        decay_steps=total_steps,
                                        use_cosine=True)
            params = network.trainable_params()
            group_params = self.set_weight_decay(params)
            if lr_schedule is not None:
                optimizer = build_optim(
                    config.optimizer,
                    default_args={"params": group_params,
                                  "learning_rate": lr_schedule})
            else:
                assert config.optimizer.learning_rate, "learning_rate must be input"
                optimizer = build_optim(
                    config.optimizer,
                    default_args={"params": group_params})

        # build callback
        if callbacks is None:
            callbacks = []
            if config.profile:
                callbacks.append(config.profile_cb)
            callbacks.extend(build_callback(
                config.callbacks, default_args={"learning_rate": optimizer.learning_rate}))

        # resume checkpoint
        if config.resume_or_finetune_checkpoint is not None and config.resume_or_finetune_checkpoint != '':
            logger.info(".............start resume training from checkpoint..................")
            resume_checkpoint_for_training(config, network, optimizer)

        # build runner wrapper
        logger.info(".........Build Running Wrapper..........")
        if wrapper is None:
            model = build_wrapper(config.runner_wrapper, default_args={"network": network, "optimizer": optimizer})
        elif isinstance(wrapper, TrainOneStepCell):
            model = wrapper
        else:
            raise NotImplementedError(f"Now not support this wrapper,"
                                      f"it should be TrainOneStepCell type, but get {wrapper}")

        # define Model and begin training
        logger.info(".........Starting Init Train Model..........")
        model = Model(model)

        model.train(
            config.runner_config.epochs, dataset, callbacks=callbacks,
            dataset_sink_mode=config.runner_config.sink_mode,
            sink_size=config.runner_config.per_epoch_size,
            initial_epoch=config.runner_config.initial_epoch)
        logger.info(".........Training Over!.............")

    def predict(self,
                config: Optional[Union[dict, ConfigArguments]] = None,
                input_data: Optional[Union[str, list, GeneratorDataset]] = None,
                network: Optional[Union[str, BaseModel]] = None,
                tokenizer: Optional[BaseTokenizer] = None,
                load_checkpoint_path: Optional[str] = None,
                **kwargs):
        """
        Executes the predict of the trainer.

        Args:
            config (Optional[Union[dict, ConfigArguments]]): The task config which is used to
                configure the dataset, the hyper-parameter, optimizer, etc.
                It support config dict or ConfigArguments class.
                Default: None.
            input_data (Optional[Union[Tensor, str, list]]): The predict data. Default: None.
            network (Optional[Union[str, BaseModel]]): The network for trainer. It support model name supported
                or BaseModel class. Supported model name can refer to model support list. For .
                Default: None.
            tokenizer (Optional[BaseTokenizer]): The tokenizer for tokenizing the input text.
                Default: None.

        Examples:
            >>> from mindformers import T5ForConditionalGeneration, TranslationTrainer
            >>> model = T5ForConditionalGeneration.from_pretrained('t5_small')
            >>> mim_trainer = TranslationTrainer(model_name="t5_small")
            >>> res = mim_trainer.predict("hello words", network=model)
            [{'translation_text': ['hello words']}]

        Returns:
            A list of prediction.

        """
        self.kwargs = kwargs

        if load_checkpoint_path is None:
            load_checkpoint_path = self.get_checkpoint_name_or_path(config)
        if load_checkpoint_path == "":
            raise ValueError("Finetune model missed, evaluation task must load finetune model!")

        param_dict = load_checkpoint(load_checkpoint_path)

        # if input_data is None:
        #     input_data = build_dataset_loader(config.eval_dataset.data_loader)

        if not isinstance(input_data, (str, list, GeneratorDataset)):
            raise ValueError("Input data's type must be one of "
                             f"[str, list, GeneratorDataset], but got type {type(input_data)}")

        if tokenizer is None:
            import os
            logger.info(os.getcwd())
            cur_path = os.getcwd()
            vocab_file_path = os.path.join(cur_path, "MyData/vocab.json")
            merge_file_path = os.path.join(cur_path, "MyData/merges.txt")
            tokenizer = GptTokenizer(vocab_file=vocab_file_path, merge_file=merge_file_path)
            # tokenizer = GptTokenizer.from_pretrained("bert_base_uncased")

        logger.info(".........Build Net..........")
        if network is None:
            # network = build_model(config.model)
            network = build_model(config.model, default_args={
                "parallel_config": config.parallel_config,
                "moe_config": config.moe_config})

        if network is not None:
            logger.info("Network Parameters: %s M.", str(count_params(network)))

        not_load_network_params = load_param_into_net(network, param_dict)
        logger.info("Not load network parameters is：%s", str(not_load_network_params))
        logger.info("Load pretrained parameter successfully!")

        save_file = kwargs.pop("save_file", None)
        if save_file is None:
            if config and config.save_file is not None:
                save_file = config.save_file
            else:
                save_file = "results.txt"

        pipeline_task = pipeline(task='translation',
                                 tokenizer=tokenizer,
                                 model=network,
                                 max_length=network.config.seq_length,
                                 **kwargs)
        output_result = pipeline_task(input_data, **kwargs)

        logger.info(".........start to write the output result to: %s.........", save_file)
        with open(save_file, 'w') as file:
            if isinstance(output_result, list):
                for item in output_result:
                    file.write(str(item) + '\n')
            else:
                file.write(str(output_result))
            file.close()

        logger.info(".........writing result finished..........")
        logger.info(".........Predict Over!.............")
        return output_result

    def evaluate(self,
                 config: Optional[Union[dict, ConfigArguments]] = None,
                 dataset: Optional[Union[str, list]] = None,
                 network: Optional[Union[str, BaseModel]] = None,
                 metric: Optional[str] = "PPL",
                 load_checkpoint_path: Optional[str] = None,
                 eval_type: Optional[str] = "finetuned",
                 **kwargs
                 ):
        self.kwargs = kwargs
        if load_checkpoint_path is None:
            load_checkpoint_path = self.get_checkpoint_name_or_path(config)
        if load_checkpoint_path == "":
            raise ValueError("Finetune model missed, evaluation task must load finetune model!")

        logger.info(".........Build Dataset..........")
        check_dataset_config(config)
        if dataset is None:
            dataset = build_dataset(config.eval_dataset_task)

        if metric.lower() == "ppl":
            logger.info(".........Prepare to calculate the ppl score..........")

            logger.info(".........Build Net..........")
            if network is None:
                network = build_model(config.model, default_args={
                    "parallel_config": config.parallel_config,
                    "moe_config": config.moe_config})
            logger.info("Network Parameters: %s M.", str(count_params(network)))

            param_dict = load_checkpoint(load_checkpoint_path)

            if eval_type == "zero-shot":
                final_param_dict = {}
                for name, _ in param_dict.items():
                    final_param_dict["gpt2." + name] = param_dict[name]
                final_param_dict["gpt2.dense1.weight"] = param_dict["gpt2.backbone.word_embedding.embedding_table"]
                not_load_network_params = load_param_into_net(network, final_param_dict)
                logger.info("Not load network parameters is：%s", str(not_load_network_params))
                logger.info("Load pretrained parameter successfully!")
            elif eval_type == "finetuned":
                not_load_network_params = load_param_into_net(network, param_dict)
                logger.info("Not load network parameters is：%s", str(not_load_network_params))
                logger.info("Load pretrained parameter successfully!")
            else:
                raise ValueError("Evaluation type missed, eval_type should be [zero-shot, finetuned]")

            model = Model(network)
            columns_list = ["input_ids", "input_mask", "label_ids"]
            logger.info(".........[PPL] Testing..........")
            num_data = 1
            total_loss = 0.0
            avg_loss = 0.0
            for data in dataset.create_dict_iterator():
                input_data = []
                for i in columns_list:
                    input_data.append(data[i])
                input_ids, input_mask, label_ids = input_data
                loss = model.predict(input_ids, input_mask, input_ids)
                loss = float(loss.asnumpy())
                total_loss += loss
                avg_loss = float(total_loss / num_data)
                logger.info(" | Current Loss {:.6f}".format(avg_loss))
                logger.info(" | Current PPL: {}\n\n".format(math.exp(avg_loss)))
                num_data += 1
            print("**************************************************************")
            print("Average Loss: {:.6f}".format(avg_loss))
            print("Average PPL: {:.6f}".format(math.exp(avg_loss)))
            print("********************** Testing Finished **********************")
        else:
            raise ValueError("metric method not supported, support: [ppl]")


