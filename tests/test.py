# from mindformers.models.opt import gpt_tokenizer_open, gpt_tokenizer
# import os
#
# print(os.getcwd())
#
# if __name__ == "__main__":
#     Tokenizer_1 = gpt_tokenizer_open.GPT2Tokenizer("/home/zhangyouwen/work/transformer/MyData/vocab.json",
#                                               "/home/zhangyouwen/work/transformer/MyData/merges.txt")
#     Tokenizer_2 = gpt_tokenizer.OptTokenizer("/home/zhangyouwen/work/transformer/MyData/vocab.json",
#                                                "/home/zhangyouwen/work/transformer/MyData/merges.txt")
#
#     string = "Limitations: Like other large language models for which the diversity (or lack thereof) of training data " \
#              "induces downstream impact on the quality of our model, OPT-175B has limitations in terms of bias and " \
#              "safety. OPT-175B can also have quality issues in terms of generation diversity and hallucination. " \
#              "In general, OPT-175B is not immune from the plethora of issues that plague modern large language models. " \
#              "By releasing with a non-commercial license, we also hope to increase communication, transparency, and " \
#              "study of the problems of large language models, especially in areas which may not be aligned with " \
#              "commercial interests. See Section 5 for a more detailed discussion of limitations of OPT-175B"
#     print(Tokenizer_1.vocab_size, Tokenizer_2.vocab_size)
#     print(Tokenizer_1.prepare_for_model(Tokenizer_1.encode(string), max_length=50), Tokenizer_2.prepare_for_model(Tokenizer_2.convert_tokens_to_ids(Tokenizer_2._tokenize(string)), max_length=50, pair_ids=[1, 2, 3, 4, 5]))


from mindformers.tools.register import MindFormerRegister

MindFormerRegister = MindFormerRegister()
print(MindFormerRegister.registry)