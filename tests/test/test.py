# from mindspore.mindrecord import FileReader, FileWriter
#
# mindrecord_file = "/home/zhangyouwen/work/transformer/tests/test/data_gen/wiki.test.mindrecord"
# reader = FileReader(file_name=mindrecord_file)
#
# schema_json = {"input_ids": {"type": "int64", "shape": [-1]},
#                    "input_mask": {"type": "int64", "shape": [-1]},
#                    "label_ids": {"type": "int64", "shape": [-1]}
#                    }
#
# writer = FileWriter(file_name="/home/zhangyouwen/work/transformer/tests/test/data_gen/wiki.test.trans.mindrecord",
#                     shard_num=1, overwrite=True)
# schema_id = writer.add_schema(schema_json, "test_schema")
#
# index = 0
# for _, item in enumerate(reader.get_next()):
#     # ori_data = item
#     index += 1
#     if index > 1:
#         break
#     writer.write_raw_data([item])
#     print(item)
# print(index)
# reader.close()
# writer.commit()

from mindspore.mindrecord import FileWriter
schema_json = {"file_name": {"type": "string"}, "label": {"type": "int32"}, "data": {"type": "bytes"}}
indexes = ["file_name", "label"]
data = [{"file_name": "1.jpg", "label": 0,
         "data": b"\x10c\xb3w\xa8\xee$o&<q\x8c\x8e(\xa2\x90\x90\x96\xbc\xb1\x1e\xd4QER\x13?\xff"},
        {"file_name": "2.jpg", "label": 56,
         "data": b"\xe6\xda\xd1\xae\x07\xb8>\xd4\x00\xf8\x129\x15\xd9\xf2q\xc0\xa2\x91YFUO\x1dsE1"},
        {"file_name": "3.jpg", "label": 99,
         "data": b"\xaf\xafU<\xb8|6\xbd}\xc1\x99[\xeaj+\x8f\x84\xd3\xcc\xa0,i\xbb\xb9-\xcdz\xecp{T\xb1"}]
writer = FileWriter(file_name="/home/zhangyouwen/work/transformer/tests/test/data_gen/wiki.test.trans.tst", shard_num=1, overwrite=True)
schema_id = writer.add_schema(schema_json, "test_schema")
status = writer.add_index(indexes)
status = writer.write_raw_data(data)
status = writer.commit()
